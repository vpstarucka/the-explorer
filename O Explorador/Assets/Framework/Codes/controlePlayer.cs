﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlePlayer : MonoBehaviour {

    private gameManager gameManager;
    private Rigidbody2D rBody;


    // Use this for initialization
    void Start () {

        gameManager = FindObjectOfType(typeof(gameManager)) as gameManager;

        rBody = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update () {

        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        float velocidade = gameManager.velocidadePersonagem;

        rBody.velocity = new Vector2(horizontal * velocidade, vertical * velocidade);

        // VERIFICA POSIÇÃO Y DO PERSONAGEM E AJUSTA CONFORME LIMITE DEFINIDO
        if (transform.position.y > gameManager.limiteMaxY)
        {
            transform.position = new Vector3(transform.position.x, gameManager.limiteMaxY, 0);
        }
        else if (transform.position.y < gameManager.limiteMinY)
        {
            transform.position = new Vector3(transform.position.x, gameManager.limiteMinY, 0);
        }

        // VERIFICA POSIÇÃO X DO PERSONAGEM E AJUSTA CONFORME LIMITE DEFINIDO
        if (transform.position.x > gameManager.limiteMaxX)
        {
            transform.position = new Vector3(gameManager.limiteMaxX, transform.position.y, 0);
        }
        else if (transform.position.x < gameManager.limiteMinX)
        {
            transform.position = new Vector3(gameManager.limiteMinX, transform.position.y, 0);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameManager.GameOver();
    }
}
