﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlataforma : MonoBehaviour {

	public GameObject platforma; // referência à plataforma a mover

	public GameObject[] pontos; // vetor dos pontos de parada da plataforma

	public float velocidade = 5f; // velocidade da plataforma
	public float espera = 1f; // tempo de espera no ponto de parada

	public bool loop = true; // volta ao início após último ponto?

	public Transform transform;
	int i = 0;		// indice do vetor pontos
	float proxTempo;  // tempo do próximo movimento
	bool seMovendo = true;

	// Inicialização
	void Start () {
		transform = platforma.transform;
		proxTempo = 0f;
		seMovendo = true;
	}

	void Update () {
		// Se passou da hora de se mover, mova-se
		if (Time.time >= proxTempo) {
			Movement();
		}
	}

	void Movement() {
		// Se há pontos para onde ir e estamos nos movendo
		if ((pontos.Length != 0) && (seMovendo)) {

			// move até o próximo ponto
			transform.position = Vector3.MoveTowards(transform.position, pontos[i].transform.position, velocidade * Time.deltaTime);

			// Se a plataforma chegou no desrino, espera
			if(Vector3.Distance(pontos[i].transform.position, transform.position) == 0) {
				i++;
				proxTempo = Time.time + espera;
			}

			// i volta para 0 se passou do limite ou paramos de nos mover se não há loop
			if(i >= pontos.Length) {
				if (loop)
					i = 0;
				else
					seMovendo = false;
			}
		}
	}
}
