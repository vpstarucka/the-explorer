﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZonaMortal : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D other) {
		if (other.gameObject.tag == "Player")
		{
			// if player then tell the player to do its FallDeath
			other.gameObject.GetComponent<Saude>().danoMax ();
		} else { // not playe so just kill object - could be falling enemy for example
			DestroyObject(other.gameObject);
		}
	}
}
