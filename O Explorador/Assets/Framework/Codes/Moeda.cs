﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moeda : MonoBehaviour {

	public int valor = 1;
	public bool foiPega = false;
	public GameObject explosao;

	// if the player touches the coin, it has not already been taken, and the player can move (not dead or victory)
	// then take the coin
	void OnTriggerEnter2D (Collider2D outro)
	{
		if ((outro.tag == "Player" ) && (!foiPega) && (!outro.gameObject.GetComponent<Saude>().morto))
		{
			// mark as taken so doesn't get taken multiple times
			foiPega=true;

			// if explosion prefab is provide, then instantiate it
			if (explosao)
			{
				Instantiate(explosao,transform.position,transform.rotation);
			}

			// do the player collect coin thing
			outro.gameObject.GetComponent<Controle>().ColetaMoeda(valor);

			// destroy the coin
			DestroyObject(this.gameObject);
		}
	}

}
