﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlePonte : MonoBehaviour {

    private gameManager gameManager;
    private Rigidbody2D rBody;

    private bool instanciado;

    // Use this for initialization
    void Start () {

        gameManager = FindObjectOfType(typeof(gameManager)) as gameManager;
        rBody = GetComponent<Rigidbody2D>();

    }
	
	// Update is called once per frame
	void Update () {

        rBody.velocity = new Vector2(gameManager.velocidadeObjetos, 0);

        if(transform.position.x <= 0 && instanciado == false)
        {
            instanciado = true;
            gameManager.instanciarPonte(transform.position.x);
        }

        if(transform.position.x <= -8)
        {
            Destroy(this.gameObject);
        }

	}
}
