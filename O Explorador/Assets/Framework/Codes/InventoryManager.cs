﻿using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    [SerializeField] Inventory inventory;
    [SerializeField] EquipmentPanel equipmentPanel;

    private void Awake()
    {
        inventory.OnItemRightClickedEvent += EquipFromInventory;
        equipmentPanel.OnItemRightClickedEvent += UnequipFromInventory;
    }

    private void EquipFromInventory(Item item)
    {
        if (item is EquippableItem)
            Equip((EquippableItem)item);
    }

    private void UnequipFromInventory(Item item)
    {
        if (item is EquippableItem)
            Unequip((EquippableItem)item);
    }

    public void Equip(EquippableItem item)
    {
        //REMOVE ITEM DO INVENTORIO
        if (inventory.RemoveItem(item))
        {
            EquippableItem previousItem;
            //PARA ADICIONAR O PAINEL DE EQUIPAMENTOS(PDE)
            //VEREFICA DE TEM ITEM NO PDE ADICIONADO AO INVETARIO
            if(equipmentPanel.AddItem(item, out previousItem))
            {
                //SE PREVIOUS TIVER ITEM ADICIONA NO INVETARIO
                if(previousItem != null)
                {
                    inventory.AddItem(previousItem);
                }
            }
            else
            {
                //AQUI SO ADICIONA O ITEM 
                inventory.AddItem(item);
            }
        }
    }

    public void Unequip(EquippableItem item)
    {
        //SE O INVETORIO TIVER ESPACO IRA ADICIONAR E REMOVER DO PDE
        if(!inventory.IsFull() && equipmentPanel.RemoveItem(item))
        {
            inventory.AddItem(item);
        }
    }
}
