﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gerenciador : MonoBehaviour {

	// referência estática ao gerenciador, para que possa ser chamado pelos outros scripts diretamente
	public static Gerenciador ger;

	// estado do jogo
	public int score = 0;

	// elementos da interface a controlar
	public Text UIScore;
	public GameObject telaDePausa;
	public GameObject[] Vidas;

	//próxima fase a (carregar se o jogador vencer a fase
	public string proximaFase;

	// Variáveis privadas
	GameObject jogador;
	Vector3 lugarDoSpawn;
	Scene cena;
	int vidas;

	public string telaGameOver;

	// Use this for initialization
	void Start () {
		// setup reference to game manager
		if (ger == null)
			ger = this.GetComponent<Gerenciador>();

		// setup all the variables, the UI, and provide errors if things not setup properly.
		setupDefaults();
	}

	void setupDefaults() {
		// setup reference to player
		if (jogador == null)
			jogador = GameObject.FindGameObjectWithTag("Player");

		if (jogador==null)
			Debug.LogError("Jogador não encontrado pelo gerenciador!");

		// get current scene
		cena = SceneManager.GetActiveScene();

		// get initial _spawnLocation based on initial position of player
		lugarDoSpawn = jogador.transform.position;

		vidas = 3;

		// if levels not specified, default to current level
		if (proximaFase=="") {
			Debug.LogWarning("levelAfterVictory not specified, defaulted to current level");
			proximaFase = cena.name;
		}

		if (telaGameOver=="") {
			Debug.LogWarning("levelAfterGameOver not specified, defaulted to current level");
			telaGameOver = cena.name;
		}

		// friendly error messages
		if (UIScore==null)
			Debug.LogError ("Need to set UIScore on Game Manager.");

		// get stored player prefs
		// refreshPlayerState();

		// get the UI ready for the game
		refreshGUI();
	}
	
	// Update is called once per frame
	void Update () {
		// if ESC pressed then pause the game
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (Time.timeScale > 0f) {
				telaDePausa.SetActive(true); // this brings up the pause UI
				Time.timeScale = 0f; // this pauses the game action
			} else {
				Time.timeScale = 1f; // this unpauses the game action (ie. back to normal)
				telaDePausa.SetActive(false); // remove the pause UI
			}
		}
	}

	// refresh all the GUI elements
	void refreshGUI() {
		// set the text elements of the UI
		UIScore.text = score.ToString();

		// turn on the appropriate number of life indicators in the UI based on the number of lives left
		for(int i=0;i<Vidas.Length;i++) {
			if (i<(vidas-1)) { // show one less than the number of lives since you only typically show lifes after the current life in UI
				Vidas[i].SetActive(true);
			} else {
				Vidas[i].SetActive(false);
			}
		}
	}

	// função que marca pontos no placar
	public void marcaPontos(int pts)
	{
		// increase score
		score+=pts;

		// update UI
		UIScore.text = score.ToString();

	}

	// perde uma vida e continua o jogo
	public void ResetGame() {
		// remove vida e atualiza interface
		vidas--;
			refreshGUI();

		if (vidas<=0) { // carrega a tela de gameover
				SceneManager.LoadScene(telaGameOver);
		} else { // respawn
			jogador.GetComponent<Controle>().Respawn(lugarDoSpawn);
		}
	}

		// função para passar de fase
		public void LevelComplete() {
			// save the current player prefs before moving to the next level
			//PlayerPrefManager.SavePlayerState(score,highscore,lives);

			// use a coroutine to allow the player to get fanfare before moving to next level
			StartCoroutine(LoadNextLevel());
		}

		// carrega a próxima fase depois de um delay
		IEnumerator LoadNextLevel() {
			yield return new WaitForSeconds(3.5f);
			SceneManager.LoadScene(proximaFase);
		}
}
