﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveOffSet : MonoBehaviour {

    private Renderer render;
    private Material materialAtual;
    public float incrementoOffset;
    public int ordemRenderizacao;
    private float offset;
    public float velocidade;


	// Use this for initialization
	void Start () {
        render = GetComponent<Renderer>();

        render.sortingOrder = ordemRenderizacao;
        materialAtual = render.material;
		
	}
	
	// Update is called once per frame
	void Update () {
        offset += incrementoOffset;
        materialAtual.SetTextureOffset("_MainTex", new Vector2(offset * velocidade, 0));

	}
}
