﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ItemSlot : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] Image image;

    public event Action<Item> OnRightClickEvent;

    private Item _item;
    //SE O MENTODO NÃO TIVER ITEM NAO MOSTRAR O IMAGEM DO ITEM NO METODO ONVANLIDADE
    public Item Item {
        get { return _item; }
    	set {
            _item = value;
            if (_item == null)
            {
                image.enabled = false;
            }
            else
            {
                image.enabled = true;
                image.sprite = _item.Icon;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //VICA VERIFICADO SE O BOTAO DIREITO DO MOUSE E CLICADO
       if(eventData != null && eventData.button == PointerEventData.InputButton.Right)
        {
            //SE O MOUSE ESTA EM SIMA DO ITEM
            //E QUANDO CLICADO COM BOTAO ATIVA O EVENTO
            //ADICIONADO O ITEM NO METADO
            //OnRightClickEvent
            if (Item != null && OnRightClickEvent != null)
                OnRightClickEvent(Item);
        }
    }

    protected virtual void OnValidate()
    {
        if(image == null)
        {
            image = GetComponent<Image>();
        }
    }
}
