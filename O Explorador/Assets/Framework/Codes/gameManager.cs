﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour {

    [Header("Personagem")]
    public Transform transformPlayer;
    public float velocidadePersonagem;

    [Header("Configuração limite movimento personagem")]
    public float limiteMaxY;
    public float limiteMinY;
    public float limiteMaxX;
    public float limiteMinX;

    [Header("Configuração da GamePlay")]
    public float velocidadeObjetos;
    public float intervaloSpawAsteroide;
    public int pontosGanhosPorAsteroide;

    [Header("Configuração da Ponte")]
    public GameObject prefabPonte;
    public float tamanhoPrefabPonte;

    [Header("Configuração do Asteroide")]
    public GameObject prefabAsteroide;
    public float posicaoXAsteroide;
    public float[] posicaoYAsteroide;
    public int[] ordemExibicao;

    [Header("HUD")]
    public Text txtPontos;
    private int pontos;

    [Header("FX")]
    public AudioSource Sfx;
    public AudioClip fxPontos;

    public void instanciarPonte(float posicaoX)
    {
        GameObject tempPonte = Instantiate(prefabPonte);
        tempPonte.transform.position = new Vector3(posicaoX + tamanhoPrefabPonte, tempPonte.transform.position.y, 0);
    }

    private void Start()
    {
        StartCoroutine("spawAsteroide");
    }

    IEnumerator spawAsteroide()
    {
        yield return new WaitForSeconds(intervaloSpawAsteroide);
        GameObject tempAsteroide = Instantiate(prefabAsteroide);
        int rand = Random.Range(0, 2);
        tempAsteroide.transform.position = new Vector3(posicaoXAsteroide, posicaoYAsteroide[rand], 0);
        tempAsteroide.GetComponent<SpriteRenderer>().sortingOrder = ordemExibicao[rand];

        StartCoroutine("spawAsteroide");
    }

    int contatorPontos = 0;

    public void pontuar()
    {

        pontos += pontosGanhosPorAsteroide;

        txtPontos.text = "PONTOS: " + pontos.ToString();

        Sfx.PlayOneShot(fxPontos, 0.7f);

        if (pontos >= 150 && pontos >= contatorPontos)
        {

            velocidadeObjetos -= 1;
            contatorPontos = pontos + 150;
            intervaloSpawAsteroide = intervaloSpawAsteroide/2;
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

}
