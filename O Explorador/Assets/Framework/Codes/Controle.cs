﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controle : MonoBehaviour {

	public int velocidade = 10;
	private bool direita = true;
	public int forçaDoPulo = 1250;
	private float moveX;

	private bool noChao;
	public LayerMask chao;
	public Transform terra;

	Gerenciador ger;

	private Animator animator;
	private Saude saude;

	public CircleCollider2D espada;
	private bool atacando;
	public float inicioEspada = 0.2f;  // Tempo que leva entre pressionar o botão e realizar o ataque
	public float duracaoEspada = 0.3f; // Duração do ataqur
	private float tempoInicioEspada = 0f;    // Tempo de inicio do ataque atual
										     // hora de "ligar" o collider da espada
	private float tempoFimEspada = 0f;       // Tempo de final do ataque atual
											 // hora de "desligar" o collider da espada

	void Start(){
		animator = gameObject.GetComponent<Animator>();
		saude = gameObject.GetComponent<Saude> ();
		espada.enabled = false;
	}


	// Update is called once per frame
	void Update () {
		if (!saude.morto) {
			moveJogador ();	
		}
	}

	void LateUpdate(){
		// DIREÇÃO DO JOGADOR
		viraJogador ();
	}

	void moveJogador(){
		// CONTROLES
		moveX = Input.GetAxis("Horizontal");
		noChao = Physics2D.Linecast (transform.position, terra.position, chao);
		if (Input.GetButtonDown("Jump") && noChao){
			pula();			
		}
		if (Input.GetButtonDown("Fire1") && !atacando){
			ataca ();
		}
		if (atacando) {
			if (!espada.enabled && Time.time >= tempoInicioEspada)
				espada.enabled = true;
			if (espada.enabled && Time.time >= tempoFimEspada) {
				espada.enabled = false;
				atacando = false;
			}
		}

		// ANIMAÇÕES
		animator.SetBool("NoChao",noChao);
		if (moveX != 0) {
			animator.SetBool ("Correndo", true);
		} else {
			animator.SetBool ("Correndo", false);
		}


		// FÍSICA
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * velocidade, 
			gameObject.GetComponent<Rigidbody2D>().velocity.y);
		Physics2D.IgnoreLayerCollision(this.gameObject.layer, 
			LayerMask.NameToLayer("Chao"), 
			(gameObject.GetComponent<Rigidbody2D>().velocity.y > 0.0f));
	}

	void pula(){
		GetComponent<Rigidbody2D> ().AddForce (Vector2.up * forçaDoPulo);
	}

	void ataca(){
		animator.SetTrigger ("Ataque");
		atacando = true;
		tempoInicioEspada = Time.time + inicioEspada;
		tempoFimEspada = tempoInicioEspada + duracaoEspada;
	}

	public void ColetaMoeda(int valor) {
		//PlaySound(SomMoeda);

		if (Gerenciador.ger) // adiciona os pontos ao gerenciador, se existir.
			Gerenciador.ger.marcaPontos(valor);
	}

	// Se o jogador colidir com uma plataforma móvel, então ele deve se tornar filho da plataforma
	// para que se mova junto com ela.
	void OnCollisionEnter2D(Collision2D outro)
	{
		if (outro.gameObject.tag=="PlataformaMovel")
		{
			this.transform.parent = outro.transform;
		}
	}

	// Se o jogador perder o contato com a plataforma móvel, deve deixar de ser seu filho
	void OnCollisionExit2D(Collision2D outro)
	{
		if (outro.gameObject.tag=="PlataformaMovel")
		{
			this.transform.parent = null;
		}
	}
		
	void viraJogador(){
		if (moveX > 0) {
			direita = true;
		} else if (moveX < 0) {
			direita = false;
		}
		Vector2 escala = transform.localScale;
		if ((escala.x > 0 && !direita) || (escala.x < 0 && direita)) {
			escala.x = escala.x * -1;
			transform.localScale = escala;
		}
	}

	// public function to respawn the player at the appropriate location
	public void Respawn(Vector3 spawnloc) {
		saude.saude = 1;
		saude.morto = false;
		transform.parent = null;
		transform.position = spawnloc;
		animator.SetTrigger ("Respawn");
	}

}
