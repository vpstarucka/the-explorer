﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controleAsteroide : MonoBehaviour
{

    private gameManager gameManager;
    private Rigidbody2D rBody;
    private bool pontuado;

    // Use this for initialization
    void Start()
    {

        gameManager = FindObjectOfType(typeof(gameManager)) as gameManager;
        rBody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {

        rBody.velocity = new Vector2(gameManager.velocidadeObjetos, 0);

        if (transform.position.x <= gameManager.transformPlayer.position.x && pontuado == false)
        {
            gameManager.pontuar();
            pontuado = true;
        }
    }
}
